package com.example.stalker;

//import java.nio.ByteBuffer;
//import java.nio.ByteOrder;
//import java.nio.FloatBuffer;
import javax.microedition.khronos.opengles.GL10;

public class Maze
{
	public int position = 0;
	
	private Color color = new Color(); 
	
	private Box box = new Box();
	
	private Cube core = new Cube(2.35f, 4.76f, 2.35f);
	
	private Cube [] cover = new Cube[4];
	
	private Walls [] walls = new Walls[4];
	
	//private float [][] wall = new float[4][];
	//private float [][] floor = new float[4][];

	//info[1] = new float[]{};
	//info[2] = new float[]{};
	//info[3] = new float[]{};
	
	
	public void initWalls()
	{
		walls[0] = new Walls(Model.wall0, 0);
		walls[1] = new Walls(Model.wall1, 1);
		walls[2] = new Walls(Model.wall2, 2);
		walls[3] = new Walls(Model.wall3, 3);
	}
	public void initCover()
	{
		float length = 4.76f;
		float width = 3.35f;
		float thick = 0.06f;
		float trans = 1.675f;
		cover[0] = new Cube(width, length, thick);
		cover[0].setTrans(0f, 0f, trans);
		cover[1] = new Cube(thick, length, width);
		cover[1].setTrans(trans, 0f, 0f);
		cover[2] = new Cube(width, length, thick);
		cover[2].setTrans(0f, 0f, -trans);
		cover[3] = new Cube(thick, length, width);
		cover[3].setTrans(-trans, 0f, 0f);
	}
	public Maze()
	{
		initWalls();
		initCover();
		//cube.setColors(cubeColors);
		core.setTrans(0, 0, 0);
	}
	
	public void setGravity(float x, float y, float z)
	{
		box.setGravity(x, y, z);
	}
	
	public void draw(GL10 gl)
	{
		gl.glEnable(GL10.GL_BLEND);
		gl.glBlendFunc(GL10.GL_SRC_ALPHA, GL10.GL_ONE_MINUS_SRC_ALPHA);
		
		
		color.setColor(gl, Color.BW);
		core.draw(gl);
		color.wipeColor(gl);
		
		for(int i=0; i<4; i++)
		{
			walls[i].setColors(Bw.colors);
			walls[i].draw(gl);
		}
		
		gl.glDepthMask(false);
		
		color.setColor(gl, Color.YGB);
		box.draw(gl);
		color.wipeColor(gl);
		
		for(int i=0; i<4; i++)
		{
			cover[i].setColors(Blue.colors);
			cover[i].draw(gl);
		}
		//color.setColor(gl, Color.YGB);
		//color.setMaterial(gl, Color.R);
		//gl.glColor4f(0.6f, 0.6f, .6f, .2f);
		
		//color.closeMaterial(gl);
		//color.wipeColor(gl);
		
		//color.setMaterial(gl, Color.R);
		
		//color.closeMaterial(gl);
		
		//gl.glColor4f(.9f, 1f, 1f, .1f);
		//cover[position].draw(gl);
		
		gl.glDepthMask(true);
	}
}