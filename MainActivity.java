package com.example.stalker;

import java.util.Timer;
import java.util.TimerTask;

import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.app.Activity;
import android.view.*;
import android.opengl.*;
import android.widget.Toast;
import android.view.GestureDetector.OnGestureListener;

public class MainActivity extends Activity implements OnGestureListener
{
	Render render = new Render();
	private GestureDetector detector = new GestureDetector(this);    
	private static Boolean isExit = false;
    private static Boolean hasTask = false;
    Timer tExit = new Timer();
    TimerTask task = new TimerTask() 
    {         
        @Override
        public void run() 
        {
            isExit = false;
            hasTask = true;
        }
    };
    
	 @Override
	 public void onCreate(Bundle savedInstanceState)
	 {
		 super.onCreate(savedInstanceState);
		 this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		 getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				 WindowManager.LayoutParams.FLAG_FULLSCREEN);
		 
		 GLSurfaceView view = new GLSurfaceView(this);
		 view.setRenderer(render);
		 setContentView(view);
		 
	     SensorManager sm=(SensorManager) this.getSystemService(SENSOR_SERVICE); 
	     Sensor sensor = sm.getDefaultSensor(Sensor.TYPE_ACCELEROMETER); 
	     SensorEventListener sel=new SensorEventListener()
	     { 
	          public void onSensorChanged(SensorEvent se)
	          { 
	               render.setGravity(se.values[SensorManager.DATA_X],
	            		   			se.values[SensorManager.DATA_Y], 
	            		   			se.values[SensorManager.DATA_Z]); 
	          } 
	          public void onAccuracyChanged(Sensor arg0, int arg1) { 
	          } 
	     }; 
	     sm.registerListener(sel, sensor,SensorManager.SENSOR_DELAY_GAME);
	 }
	 
	 public boolean onKeyDown(int keyCode, KeyEvent event) 
	 {
		 if(keyCode == KeyEvent.KEYCODE_BACK)
	     {
			 if(isExit == false ) 
			 {
	             isExit = true;
	             Toast.makeText(this, "再按一次退出游戏", Toast.LENGTH_SHORT).show();  
	             if(!hasTask) 
	             {
	            	 tExit.schedule(task, 1000);
	             }                
			 }
			 else 
	         {                                 
				 finish();  
				 System.exit(0);  
	         }
	     }            
	     return false;
	 }

	@Override
	public boolean onTouchEvent(MotionEvent event) {
		return this.detector.onTouchEvent(event);
	}
	
	@Override
	public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX,	float velocityY) {
		if(e2.getX()-e1.getX() > 120)
		{
			render.rotate(false);
			return true;
		}
		else if(e1.getX()-e2.getX() > 120)
		{
			render.rotate(true);
			return true;
		}
		return false;
	}
	
	@Override
	public boolean onDown(MotionEvent arg0) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean onScroll(MotionEvent arg0, MotionEvent arg1, float arg2,
			float arg3) {
		// TODO Auto-generated method stub 
		return false;
	}

	@Override
	public void onShowPress(MotionEvent arg0) {
		// TODO Auto-generated method stub
	}

	@Override
	public boolean onSingleTapUp(MotionEvent arg0) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void onLongPress(MotionEvent e) {
		// TODO Auto-generated method stub
	}
}