package com.example.stalker;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.nio.ShortBuffer;

import javax.microedition.khronos.opengles.GL10;

public class Mesh
{
    private FloatBuffer verticesBuffer = null;
    private ShortBuffer indicesBuffer = null;
    private FloatBuffer colorBuffer = null;
    
    private int numOfIndices = -1;
    private float[] trans = null;
    
    public void draw(GL10 gl) 
    {
		gl.glFrontFace(GL10.GL_CCW);
		gl.glEnable(GL10.GL_CULL_FACE);
		gl.glCullFace(GL10.GL_BACK);
		
		gl.glEnableClientState(GL10.GL_VERTEX_ARRAY);
		gl.glVertexPointer(3, GL10.GL_FLOAT, 0, verticesBuffer);

	    if ( colorBuffer != null )
	    {
	    	gl.glEnableClientState(GL10.GL_COLOR_ARRAY);
	        gl.glColorPointer(4, GL10.GL_FLOAT, 0, colorBuffer);
	    }
	    
	    gl.glPushMatrix();
	    if( trans != null )
	    	gl.glTranslatef(trans[0], trans[1], trans[2]);
		gl.glDrawElements(GL10.GL_TRIANGLES, numOfIndices,
			GL10.GL_UNSIGNED_SHORT, indicesBuffer);
		gl.glPopMatrix();
		
		gl.glDisableClientState(GL10.GL_COLOR_ARRAY);
		gl.glDisableClientState(GL10.GL_VERTEX_ARRAY);
		gl.glDisable(GL10.GL_CULL_FACE);
    }
    
    protected void setVertices(float[] vertices)
    {
    	ByteBuffer vbb = ByteBuffer.allocateDirect(vertices.length*4);
    	vbb.order(ByteOrder.nativeOrder());
    	verticesBuffer = vbb.asFloatBuffer();
    	verticesBuffer.put(vertices);
    	verticesBuffer.position(0);
    }
     
    protected void setIndices(short[] indices) 
    {
    	ByteBuffer ibb = ByteBuffer.allocateDirect(indices.length*2);
    	ibb.order(ByteOrder.nativeOrder());
    	indicesBuffer = ibb.asShortBuffer();
    	indicesBuffer.put(indices);
    	indicesBuffer.position(0);
    	numOfIndices = indices.length;
    }
    
    protected void setColors(float[] colors) 
    {
    	ByteBuffer cbb = ByteBuffer.allocateDirect(colors.length * 4);
    	cbb.order(ByteOrder.nativeOrder());
    	colorBuffer = cbb.asFloatBuffer();
    	colorBuffer.put(colors);
    	colorBuffer.position(0);
    }
    
    protected void setTrans(float x, float y, float z)
    {
    	trans = new float[3];
    	trans[0] = x;
    	trans[1] = y;
    	trans[2] = z;
    }
}