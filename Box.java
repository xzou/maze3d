package com.example.stalker;

import javax.microedition.khronos.opengles.GL10;

public class Box
{
	private final float a = 0.33f;
	private final Cube box = new Cube(a, a, a);
	private final float sqrt2 = (float)Math.sqrt(2);
	private final float degree45 = (float)Math.PI/4;
	private final float degree90 = (float)Math.PI/2;
	
	private short x = 0;
	private short y = 1;
	private short position = 0;
	private short direction = -1;
	private float [] trans = {0f, 0.235f, 1.385f};
	
	private float t = 0f;
	private boolean rotating = false;
	private float [] rot = {0, 0, 0};
	
	public void draw(GL10 gl)
	{
		update();
		gl.glPushMatrix();
		gl.glTranslatef(trans[0], trans[1], trans[2]);
		gl.glRotatef(rot[0], 1, 0, 0);
		gl.glRotatef(rot[1], 0, 1, 0);
		gl.glRotatef(rot[2], 0, 0, 1);
		box.draw(gl);
		gl.glPopMatrix();
	}

	void update()
	{
		float tz = 1.385f;
		float tx = (x/2f)*Walls.margin;
		float ty = (y/2f)*Walls.margin;
		if(rotating == false)
		{
			trans[1]=ty;
			switch(position)
			{
			case 0: trans[0]=tx;  trans[2]=tz; break;
			case 1: trans[0]=tz; trans[2]=-tx; break;
			case 2: trans[0]=-tx; trans[2]=-tz; break;
			case 3: trans[0]=-tz; trans[2]=tx; break;
			}
			return;
		}
		
		if(Math.abs(t-1f) > 0.001f)
			t += 0.1f;
		else
			t = 1f;
		float angle = (float) (t*degree90);
		
		
		
		float dx = Walls.margin*(sqrt2*(float)Math.cos(degree45-angle)-1)/2;
		float dy = Walls.margin*(1-sqrt2*(float)Math.sin(degree45-angle))/2;
		
		if(position == 0)
		{
			if(direction==0 || direction==2)
			{
				trans[0] = tx;
				trans[1] = ty - dy;
				trans[2] = tz + dx;
				rot[0] += 9f;
				if(direction == 2)
				{
					trans[1] = ty + dy;
					rot[0] -= 18f;
				}
			}
			if(direction==1 || direction==3)
			{
				trans[0] = tx + dy;
				trans[1] = ty;
				trans[2] = tz + dx;
				rot[1] += 9f;
				if(direction == 3)
				{
					trans[0] = tx - dy;
					rot[1] -= 18f;
				}
			}
		}
		else if(position == 1)
		{
			if(direction==0 || direction==2)
			{
				trans[2] = -tx;
				trans[1] = ty - dy;
				trans[0] = tz + dx;
				rot[2] -= 9f;
				if(direction == 2)
				{
					trans[1] = ty + dy;
					rot[2] += 18f;
				}
			}
			if(direction==1 || direction==3)
			{
				trans[2] = -(tx + dy);
				trans[1] = ty;
				trans[0] = tz + dx;
				rot[1] += 9f;
				if(direction == 3)
				{
					trans[2] = -tx + dy;
					rot[1] -= 18f;
				}
			}
		}
		else if(position == 2)
		{
			if(direction==0 || direction==2)
			{
				trans[0] = -tx;
				trans[1] = ty - dy;
				trans[2] = -(tz + dx);
				rot[0] -= 9f;
				if(direction == 2)
				{
					trans[1] = ty + dy;
					rot[0] += 18f;
				}
			}
			if(direction==1 || direction==3)
			{
				trans[0] = -(tx + dy);
				trans[1] = ty;
				trans[2] = -(tz + dx);
				rot[1] += 9f;
				if(direction == 3)
				{
					trans[0] = -(tx - dy);
					rot[1] -= 18f;
				}
			}
		}
		else
		{
			if(direction==0 || direction==2)
			{
				trans[2] = tx;
				trans[1] = ty - dy;
				trans[0] = -(tz + dx);
				rot[2] += 9f;
				if(direction == 2)
				{
					trans[1] = ty + dy;
					rot[2] -= 18f;
				}
			}
			if(direction==1 || direction==3)
			{
				trans[2] = tx + dy;
				trans[1] = ty;
				trans[0] = -(tz + dx);
				rot[1] += 9f;
				if(direction == 3)
				{
					trans[2] = tx - dy;
					rot[1] -= 18f;
				}
			}
		}
		
		if(Math.abs(t-1f) < 0.001)
		{
			t = 0f;
			rotating = false;
			switch(direction)
			{
				case 0: y -= 2; break;
				case 1: x += 2; break;
				case 2: y += 2; break;
				case 3: x -= 2; break;
			}
			fall();
		}
	}
	
	void fall()
	{
		if(x == 6)
		{
			switch(position)
			{
			case 0: fallable(Model.wall1, true); break;
			case 1: fallable(Model.wall2, true); break;
			case 2: fallable(Model.wall3, true); break;
			case 3: fallable(Model.wall0, true); break;
			}
		}
		else if(x == -6)
		{
			switch(position)
			{
			case 0: fallable(Model.wall3, false); break;
			case 1: fallable(Model.wall0, false); break;
			case 2: fallable(Model.wall1, false); break;
			case 3: fallable(Model.wall2, false); break;
			}
		}
		else
			return;
	}
	
	boolean fallable(short [] a, boolean right)
	{
		if(right == true)
		{
			short min = 7;
			for(int i=0; i<a.length; i+=4)
			{
				if(a[i+1] == a[i+3])
					continue;
				if(y<a[i+1] && y>a[i+3] && a[i]<min)
					min = a[i];
			}
			if(min == -5)
				return false;
			else
			{
				position = (short) ((position+1)%4);
				x = (short) (min-1);
				return true;
			}
		}
		else
		{
			short max = -7;
			for(int i=0; i<a.length; i+=4)
			{
				if(a[i+1] == a[i+3])
					continue;
				if(y<a[i+1] && y>a[i+3] && a[i]>max)
					max = a[i];
			}
			if(max == 5)
				return false;
			else
			{
				position = (short) ((position+3)%4);
				x = (short) (max+1);
				return true;
			}
		}
	}
	
	public void setGravity(float x, float y, float z)
	{
		if(rotating == true)
			return;
		rotating = true;
		if(Math.abs(x) > Math.abs(y))
		{
			direction = (short)(x>0?3:1);
		}
		else
		{
			direction = (short)(y>0?0:2);
		}
		short [] bars = null;
		switch(position)
		{
		case 0: bars = Model.wall0; break; 
		case 1: bars = Model.wall1; break;
		case 2: bars = Model.wall2; break;
		case 3: bars = Model.wall3; break;
		}
		if(!access(bars))
			rotating = false;
	}

	boolean access(short [] a)
	{
		synchronized(this)
		{
			if(direction==0 || direction==2)
			{
				if(direction == 0)
				{
					if(y == -9)
						return false;
				}
				else
				{
					if(y == 9)
						return false;
				}
				short point = (short)(direction==0?y-1:y+1);
				for(int i=0; i<a.length; i+=4)
				{
					if(a[i+1]==point && x>a[i] && x<a[i+2])
						return false;
					if(a[i] == a[i+2])
						break;
				}
			}
			else if(direction==1 || direction==3)
			{
				if(direction == 1)
				{
					if(x == 6)
						return false;
				}
				else
				{
					if(x == -6)
						return false;
				}
				short point = (short)(direction==1?x+1:x-1);
				for(int i=0; i<a.length; i+=4)
				{
					if(a[i+1] == a[i+3])
						continue;
					if(a[i]==point && y<a[i+1] && y>a[i+3])
						return false;
				}
			}
			return true;
		}
	}
}