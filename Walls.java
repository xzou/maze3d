package com.example.stalker;

//import java.math.*;
import java.nio.FloatBuffer;
import java.util.Vector;
import javax.microedition.khronos.opengles.GL10;
 
public class Walls
{
    private Vector<Mesh> walls = new Vector<Mesh>();
    int numOfWalls;
    final static float thickness = 0.06f;
    final static float height = 0.47f;
    final static float margin = 0.47f;
    final static float shift = 1.41f;
    
    float shininess = 0f;
	private FloatBuffer [] materialBuffer = new FloatBuffer[3];

	Walls(short [] a, int position)
	{
		float [] dim = new float[3];
		float [] trans = new float[3];
		Cube ele;
		boolean level;
		int length;
		float middle;
		for(int i=0; i<a.length; i+=4)
		{
			length = Math.abs(a[i]-a[i+2]);
			if(length == 0)
			{
				level = false;
				middle = (a[i+1]+a[i+3])/2;
				length = Math.abs(a[i+1]-a[i+3]);
			}
			else
			{
				level = true;
				middle = (a[i]+a[i+2])/2;
			}
			if(position == 0)
			{
				dim[2] = height;
				trans[2] = shift;
				if(level == true)
				{
					dim[0] = length*margin/2+thickness;
					dim[1] = thickness;
					trans[0] = (middle/2)*margin;
					trans[1] = (a[i+1]/2f)*margin;
				}
				else
				{
					dim[0] = thickness;
					dim[1] = length*margin/2+thickness;
					trans[0] = (a[i]/2f)*margin;
					trans[1] = (middle/2)*margin;
				}
			}
			else if(position == 1)
			{
				dim[0] = height;
				trans[0] = shift;
				if(level == true)
				{
					dim[1] = thickness;
					dim[2] = length*margin/2+thickness;
					trans[1] = (a[i+1]/2f)*margin;
					trans[2] = -(middle/2)*margin;
				}
				else
				{
					dim[2] = thickness;
					dim[1] = length*margin/2+thickness;
					trans[2] = -(a[i]/2f)*margin;
					trans[1] = (middle/2)*margin;
				}
			}
			else if(position == 2)
			{
				dim[2] = height;
				trans[2] = -shift;
				if(level == true)
				{
					dim[0] = length*margin/2+thickness;
					dim[1] = thickness;
					trans[0] = -(middle/2f)*margin;
					trans[1] = (a[i+1]/2)*margin;
				}
				else
				{
					dim[0] = thickness;
					dim[1] = length*margin/2+thickness;
					trans[0] = -(a[i]/2f)*margin;
					trans[1] = (middle/2)*margin;
				}
			}
			else
			{
				dim[0] = height;
				trans[0] = -shift;
				if(level == true)
				{
					dim[1] = thickness;
					dim[2] = length*margin/2+thickness;
					trans[1] = (a[i+1]/2f)*margin;
					trans[2] = (middle/2)*margin;
				}
				else
				{
					dim[2] = thickness;
					dim[1] = length*margin/2+thickness;
					trans[2] = (a[i]/2f)*margin;
					trans[1] = (middle/2)*margin;
				}
			}
			ele = new Cube(dim[0], dim[1], dim[2]);
			ele.setTrans(trans[0], trans[1], trans[2]);
			walls.add(ele);
		}
	}
	
    public void draw(GL10 gl) 
    {
        numOfWalls = walls.size();
        for(int i = 0; i < numOfWalls; i ++)
            walls.get(i).draw(gl);
    }
    
    public void setMaterial(float [] ambient, float [] diffuse, float [] specular, float shininess)
    {
    	materialBuffer[0] = Render.toFloatBuffer(ambient);
    	materialBuffer[1] = Render.toFloatBuffer(diffuse);
    	materialBuffer[2] = Render.toFloatBuffer(specular);
    	this.shininess = shininess;
    }
 
    public void setColors(float [] color)
    {
    	numOfWalls = walls.size();
    	for(int i = 0; i < numOfWalls; i ++)
    		walls.get(i).setColors(color);
    }
    
    public void add(int location, Mesh object) 
    {
        walls.add(location, object);
    }
 
    public boolean add(Mesh object)
    {
        return walls.add(object);
    }
 
    public void clear()
    {
        walls.clear();
    }
 
    public Mesh get(int location)
    {
        return walls.get(location);
    }
 
    public Mesh remove(int location)
    {
        return walls.remove(location);
    }
 
    public boolean remove(Object object)
    {
        return walls.remove(object);
    }
 
    public int size()
    {
        return walls.size();
    }
}
