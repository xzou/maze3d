package com.example.stalker;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
//import java.math.*;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

import android.opengl.GLSurfaceView.*;
import android.opengl.GLU;

public class Render implements Renderer
{
	Maze maze = new Maze();
	
	private float degree90 = (float)Math.PI/2;
	private float angle = degree90;
	private final float radius = 7.5f;
	private boolean rotating = false;
	private boolean clock = false;
	
	private float [] viewPoint = {0f, 0f, radius};
	//private float [] lightAmbientPointer = {.2f, .2f, .2f, 1f};
	//private float [] lightColorPointer = {.8f, .8f, .8f, 1f};
	//private float [] lightPosPointer0 = {-3f, 5f, 3f, 1f};
	//private float [] lightPosPointer1 = {-3f, 3f, -3f, 1f};
	//private FloatBuffer lightColor, lightPosition0, lightPosition1, lightAmbient;
	
	public static FloatBuffer toFloatBuffer(float [] a)
	{
		ByteBuffer bb = ByteBuffer.allocateDirect(a.length*4);
    	bb.order(ByteOrder.nativeOrder());
    	FloatBuffer buf = bb.asFloatBuffer();
    	buf.put(a);
    	buf.position(0);
    	return buf;
	}
	
	public void setGravity(float x, float y, float z)
	{
		if(Math.abs(x)>3 || Math.abs(y)>3)
			maze.setGravity(x, y, z);
	}
	
	public void rotate(boolean left)
	{
		rotating = true;
		if(left == true)
		{
			clock = true;
			maze.position = (maze.position+1)%4;
		}
		else
		{
			clock = false;
			maze.position = (maze.position+3)%4;
		}
	}
	
	void updateViewPoint(GL10 gl)
	{
		angle += clock?-0.1:0.1;
		if(Math.abs(Math.cos(angle)) < 0.1)
		{
			if(maze.position==0 || maze.position==2)
			{
				angle = maze.position==0?degree90:-degree90;
				rotating = false;
			}
		}
		else if(Math.abs(Math.sin(angle)) < 0.1)
		{
			if(maze.position==1 || maze.position==3)
			{
				angle = maze.position==1?0:2*degree90;
				rotating = false;
			}
		}
		viewPoint[0] = (float) (Math.cos(angle)*radius);
		viewPoint[2] = (float) (Math.sin(angle)*radius);
		//lightPosPointer1[0] = (float) (Math.cos(angle+degree90/2)*radius)/2;
		//lightPosPointer1[2] = (float) (Math.sin(angle+degree90/2)*radius)/2;
		//lightPosition1 = toFloatBuffer(lightPosPointer1);
		//gl.glLightfv(GL10.GL_LIGHT0, GL10.GL_POSITION, lightPosition1);
	}
	
	 public void onSurfaceCreated(GL10 gl, EGLConfig config) 
	 {		 
		 //lightColor = toFloatBuffer(lightColorPointer);
		 //lightPosition0 = toFloatBuffer(lightPosPointer0);
		 //lightPosition1 = toFloatBuffer(lightPosPointer1);
		 //lightAmbient = toFloatBuffer(lightAmbientPointer);
		 // Set the background color to black ( rgba ).
		 gl.glClearColor(0f, 0f, 0f, 0.5f);
		 // Enable Smooth Shading, default not really needed.
		 gl.glShadeModel(GL10.GL_SMOOTH);
		 // Depth buffer setup.
		 gl.glClearDepthf(1.0f);
		 // Enables depth testing.
		 gl.glEnable(GL10.GL_DEPTH_TEST);
		 // The type of depth testing to do.
		 gl.glDepthFunc(GL10.GL_LEQUAL);
		 // Really nice perspective calculations.
		 gl.glHint(GL10.GL_PERSPECTIVE_CORRECTION_HINT,GL10.GL_NICEST);
		 
		 // Lighting settings
		 //gl.glEnable(GL10.GL_LIGHTING);
		 //gl.glEnable(GL10.GL_LIGHT0);
		 //gl.glEnable(GL10.GL_CULL_FACE);
		 //gl.glLightfv(GL10.GL_LIGHT0, GL10.GL_AMBIENT, lightAmbient);
		 //gl.glLightfv(GL10.GL_LIGHT0, GL10.GL_DIFFUSE, lightColor);
		 //gl.glLightfv(GL10.GL_LIGHT0, GL10.GL_SPECULAR, lightColor);
		 //gl.glLightfv(GL10.GL_LIGHT0, GL10.GL_POSITION, lightPosition0);
		 //gl.glLightfv(GL10.GL_LIGHT1, GL10.GL_AMBIENT, lightAmbient);
		 //gl.glLightfv(GL10.GL_LIGHT1, GL10.GL_DIFFUSE, lightColor);
		 //gl.glLightfv(GL10.GL_LIGHT1, GL10.GL_SPECULAR, lightColor);
		 //gl.glLightfv(GL10.GL_LIGHT1, GL10.GL_POSITION, lightPosition1);
		 //gl.glLightfv(GL10.GL_LIGHT0, GL10.GL_SPOT_DIRECTION, lightDirection);
		 //gl.glLightf(GL10.GL_LIGHT0, GL10.GL_SPOT_EXPONENT, 0f);
		 //gl.glLightf(GL10.GL_LIGHT0, GL10.GL_SPOT_CUTOFF, 45f);
	 }
	  
	 public void onDrawFrame(GL10 gl) 
	 {
		 // Clears the screen and depth buffer.
		 gl.glClear(GL10.GL_COLOR_BUFFER_BIT | GL10.GL_DEPTH_BUFFER_BIT);
		 gl.glLoadIdentity();
		 GLU.gluLookAt(gl, viewPoint[0], viewPoint[1], viewPoint[2], 0f, 0f, 0f, 0f, 1f, 0f);
		 gl.glScalef(0.9f, 0.9f, 0.9f);
		 maze.draw(gl);
		 if(rotating == true)
		 	updateViewPoint(gl);
	 }
	  
	 public void onSurfaceChanged(GL10 gl, int width, int height) 
	 {
		 // Sets the current view port to the new size.
		 gl.glViewport(0, 0, width, height);
		 // Select the projection matrix
		 gl.glMatrixMode(GL10.GL_PROJECTION);
		 // Reset the projection matrix
		 gl.glLoadIdentity();
		 // Calculate the aspect ratio of the window
		 GLU.gluPerspective(gl, 45.0f, width/(float)height, 0.1f, 100.0f);
		 // Select the model view matrix
		 gl.glMatrixMode(GL10.GL_MODELVIEW);
		 // Reset the model view matrix
		 gl.glLoadIdentity();
	 }
}