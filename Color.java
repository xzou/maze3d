package com.example.stalker;

import java.nio.FloatBuffer;
import javax.microedition.khronos.opengles.GL10;

class Silver
{
	static final float [] ambient = {0.192250f, 0.192250f, 0.192250f, 1.000000f};
	static final float [] diffuse = {0.507540f, 0.507540f, 0.507540f, 1.000000f};
	static final float [] specular = {0.508273f, 0.508273f, 0.508273f, 1.000000f};
	static final float [] shininess = {51.2f};
}

class Ruby
{
	static final float [] ambient = {0.174500f, 0.011750f, 0.011750f, 0.550000f};
	static final float [] diffuse = {0.614240f, 0.041360f, 0.041360f, 0.550000f};
	static final float [] specular = {0.727811f, 0.626959f, 0.626959f, 0.550000f};
	static final float [] shininess = {76.800003f};
}

class Ygb
{
	static final float [] colors =
	{
		0.0f, 0.5f, 0.5f, 0.6f,
		0.0f, 0.5f, 0.5f, 0.6f,
		0.0f, 0.5f, 0.0f, 0.6f,
		0.0f, 0.0f, 0.5f, 0.6f,
		0.5f, 0.5f, 0.0f, 0.6f,
		0.0f, 0.0f, 0.5f, 0.6f,
		0.5f, 0.5f, 0.0f, 0.6f,
		0.0f, 0.5f, 0.0f, 0.6f
	};
}

class Blue
{
	static final float [] colors =
	{
			.6f, 0.0f, 1f, .1f,
			1f, 0.0f, .6f, .1f,
			.6f, 0.0f, 1f, .1f,
			1f, 0.0f, .6f, .1f,
			1f, 0.0f, .6f, .1f,
			.6f, 0.0f, 1f, .1f,
			1f, 0.0f, .6f, .1f,
			.6f, 0.0f, 1f, .1f
	};
}

class Bw
{
	static final float [] colors =
	{
		0f, 0f, 0f, .7f,
		1f, 1f, 1f, .7f,
		0f, 0f, 0f, .7f,
		1f, 1f, 1f, .7f,
		1f, 1f, 1f, .7f,
		0f, 0f, 0f, .7f,
		1f, 1f, 1f, .7f,
		0f, 0f, 0f, .7f,
	};
}
public class Color
{
	static final short S = 0;
	static final short R = 3;
	static final short YGB = 1;
	static final short BW = 2;
	static final short B = 2;
	private FloatBuffer [] silver = new FloatBuffer[4];
	private FloatBuffer [] ruby = new FloatBuffer[4]; 
	private FloatBuffer ygb;
	private FloatBuffer bw;
	private FloatBuffer b;
	
	Color()
	{
		silver[0] = Render.toFloatBuffer(Silver.ambient);
		silver[1] = Render.toFloatBuffer(Silver.diffuse);
		silver[2] = Render.toFloatBuffer(Silver.specular);
		silver[3] = Render.toFloatBuffer(Silver.shininess);
		
		ruby[0] = Render.toFloatBuffer(Silver.ambient);
		ruby[1] = Render.toFloatBuffer(Silver.diffuse);
		ruby[2] = Render.toFloatBuffer(Silver.specular);
		ruby[3] = Render.toFloatBuffer(Silver.shininess);
		
		bw = Render.toFloatBuffer(Bw.colors);
		ygb = Render.toFloatBuffer(Ygb.colors);
		b = Render.toFloatBuffer(Blue.colors);
	}
	
	void setMaterial(GL10 gl, short material)
	{
		gl.glEnable(GL10.GL_LIGHTING);
		gl.glEnable(GL10.GL_LIGHT0);
		//gl.glEnable(GL10.GL_LIGHT1);
		if(material == S)
		{
	    	 gl.glMaterialfv(GL10.GL_FRONT, GL10.GL_AMBIENT, silver[0]);
			 gl.glMaterialfv(GL10.GL_FRONT_AND_BACK, GL10.GL_DIFFUSE, silver[1]);
			 gl.glMaterialfv(GL10.GL_FRONT, GL10.GL_SPECULAR, silver[2]);
			 gl.glMaterialfv(GL10.GL_FRONT_AND_BACK, GL10.GL_SHININESS, silver[3]);
		}
		if(material == R)
		{
	    	 gl.glMaterialfv(GL10.GL_FRONT, GL10.GL_AMBIENT, ruby[0]);
			 gl.glMaterialfv(GL10.GL_FRONT_AND_BACK, GL10.GL_DIFFUSE, ruby[1]);
			 gl.glMaterialfv(GL10.GL_FRONT, GL10.GL_SPECULAR, ruby[2]);
			 gl.glMaterialfv(GL10.GL_FRONT_AND_BACK, GL10.GL_SHININESS, ruby[3]);
		}
	}
	void setColor(GL10 gl, short color)
	{
		gl.glEnableClientState(GL10.GL_COLOR_ARRAY);
		if(color == YGB)
		{
	        gl.glColorPointer(4, GL10.GL_FLOAT, 0, ygb);
		}
		else if(color == BW)
		{
			gl.glColorPointer(4, GL10.GL_FLOAT, 0, bw);
		}
	}
	void wipeColor(GL10 gl)
	{
		gl.glDisableClientState(GL10.GL_COLOR_ARRAY);
	}
	void closeMaterial(GL10 gl)
	{
		gl.glDisable(GL10.GL_LIGHT0);
		gl.glDisable(GL10.GL_LIGHT1);
    	gl.glDisable(GL10.GL_LIGHTING);
	}
	
}